package chaosmage.csv

object CsvLine {
  def apply(element: String*) = new CsvLine(element.toVector)
}

case class CsvLine(columns: Vector[String]) {
  def :+ (value: String) = CsvLine(columns :+ value)
}
