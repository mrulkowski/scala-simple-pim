package chaosmage.csv

case class CsvConfiguration(enclosedBy: Char, separatedBy: Char)
