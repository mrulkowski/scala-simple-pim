package chaosmage.csv

object CsvParser {
  sealed trait State
  case class None() extends State
  case class Separator() extends State

  case class EnclosedColumn(value: String) extends State {
    def + (char: Char) = EnclosedColumn(value + char)
  }
  case class Column(value: String) extends State {
    def + (char: Char) = Column(value + char)
  }
  case class ClosedColumn(value: String) extends State
}


/**
  * Pull parser where source must return complete line.
  *
  * @param config - configuration of this csv parser
  */
class CsvParser(val config: CsvConfiguration) {

  import CsvParser._

  private case class IncompleteLine()

  def parse(lines: Stream[String]): Stream[CsvLine] = {
    lines match {
      case Stream.Empty => Stream.empty[CsvLine]
      case head #:: tail => parseStream(head, tail) match {
        case (parsed, remainingTail) => Stream.cons(parsed, parse(remainingTail))
      }
    }
  }

  private def parseStream(head: String, tail: Stream[String]): (CsvLine, Stream[String]) = {
    consume(head.toList, None(), CsvLine()) match {
      case Right(value) => (value, tail)
      case Left(_) => tail match {
        case Stream.Empty => (CsvLine(), Stream.empty) // TODO - in this case we should log this problem in formatting
        case tailHead #:: tailTail => parseStream(head + "\n" + tailHead, tailTail)
      }
    }
  }

  def parse(line: String): CsvLine = {
    consume(line.toList, None(), CsvLine()).right.getOrElse(CsvLine())
  }

  private def consume[S <: State](remaining: List[Char], lastState: S, found: CsvLine): Either[IncompleteLine, CsvLine] = {
    remaining match {
      case Nil => lastState match {
        case None() => Right(found)
        case EnclosedColumn(value) => Left(IncompleteLine())
        case Column(value) => Right(found :+ value)
        case ClosedColumn(value) => Right(found :+ value)
        case Separator() => Right(found :+ "")
      }
      case x :: xs => lastState match {
        case None() => x match {
          case config.separatedBy => consume(xs, Separator(), found :+ "")
          case config.enclosedBy => consume(xs, EnclosedColumn(""), found)
          case ' ' => consume(xs, None(), found)
          case _ => consume(xs, Column("") + x, found)
        }
        case Separator() => x match {
          case ' ' => consume(xs, Separator(), found)
          case _ => consume(remaining, None(), found)
        }
        case col @ EnclosedColumn(value) => (x, xs) match {
          case (config.enclosedBy, config.enclosedBy :: ys) => consume(ys, col + config.enclosedBy, found)
          case (config.enclosedBy, _) => consume(xs, ClosedColumn(value), found)
          case (_, _) => consume(xs, col + x, found)
        }
        case col @ ClosedColumn(value) => x match {
          case config.separatedBy => consume(xs, Separator(), found :+ value)
          case _ => consume(xs, col, found)
        }
        case col @ Column(value) => x match {
          case config.separatedBy => consume(xs, Separator(), found :+ value.trim)
          case _ => consume(xs, col + x, found)
        }
      }
    }
  }

}
