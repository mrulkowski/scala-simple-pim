package chaosmage.cassandra

import com.datastax.driver.core.{Cluster, Session}

trait CassandraSessionComponent {

  def cluster: Cluster

  def session: Session

}
