package chaosmage.cassandra

import java.net.InetAddress

import com.typesafe.config.Config
import scala.collection.JavaConversions._

final class Configuration(config: Config) {

  lazy val contactPoints: List[InetAddress] = {
    val points = config.getStringList("points").toList

    points.map((host) => InetAddress.getByName(host))
  }

  lazy val clusterName: String = {
    config.getString("clusterName")
  }

  lazy val port: Int = {
    config.getInt("port")
  }

  lazy val keySpace: String = {
    config.getString("keySpace")
  }

  lazy val credentials: Option[Credentials] = {
    if (config.hasPath("credentials.username") && config.hasPath("credentials.password")) {
      val username = config.getString("credentials.username")
      val password = config.getString("credentials.password")

      Option(Credentials(username, password))
    } else {
      None
    }
  }
}
