package chaosmage.cassandra

case class Credentials(username: String, password: String)
