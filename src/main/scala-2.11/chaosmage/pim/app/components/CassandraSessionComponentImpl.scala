package chaosmage.pim.app.components

import chaosmage.cassandra.{CassandraSessionComponent, Configuration}
import com.datastax.driver.core.{Cluster, Session}

import scala.collection.JavaConversions._

trait CassandraSessionComponentImpl extends CassandraSessionComponent {
  this: ConfigurationComponent ⇒

  lazy val cluster: Cluster = {
    val cassandra = new Configuration(config.getConfig("pim.cassandra"))

    val clusterBuilder = Cluster
      .builder()
      .withClusterName(cassandra.clusterName)
      .withPort(cassandra.port)
      .addContactPoints(cassandra.contactPoints)

    cassandra.credentials.foreach(credentials ⇒
      clusterBuilder.withCredentials(credentials.username, credentials.password)
    )

    clusterBuilder.build()
  }

  lazy val session: Session = {
    val cassandra = new Configuration(config.getConfig("pim.cassandra"))

    cluster.connect(cassandra.keySpace)
  }
}
