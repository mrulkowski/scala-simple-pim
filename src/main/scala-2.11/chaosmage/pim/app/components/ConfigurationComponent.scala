package chaosmage.pim.app.components

import com.typesafe.config.{Config, ConfigFactory}

trait ConfigurationComponent {
  def config: Config
}

trait FileConfigurationComponent extends ConfigurationComponent {
  val config = ConfigFactory.load()
}
