package chaosmage.pim.app.components

import akka.actor.ActorSystem

trait ActorComponent {

  def actorSystem: ActorSystem

}

trait ActorComponentImpl extends ActorComponent {
  this: ConfigurationComponent ⇒


  lazy val actorSystem = {
    ActorSystem(config.getString("pim.actor-system.name"), config.getConfig("pim.akka"))
  }

}