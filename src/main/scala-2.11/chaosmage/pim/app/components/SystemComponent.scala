package chaosmage.pim.app.components

import chaosmage.cassandra.CassandraSessionComponent

trait SystemComponent
  extends ConfigurationComponent
    with ActorComponent
    with CassandraSessionComponent
