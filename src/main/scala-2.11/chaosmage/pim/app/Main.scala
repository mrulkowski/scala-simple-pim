package chaosmage.pim.app

import akka.io.IO
import akka.util.Timeout

import scala.concurrent.duration._
import chaosmage.pim.app.components.{ActorComponentImpl, CassandraSessionComponentImpl, FileConfigurationComponent, SystemComponent}
import akka.pattern.ask
import chaosmage.pim.http.HttpListener
import spray.can.Http

import scala.language.postfixOps

object Main extends App {

  implicit val concreteSystem = new SystemComponent
    with CassandraSessionComponentImpl
    with FileConfigurationComponent
    with ActorComponentImpl


  implicit val akkaSystem = concreteSystem.actorSystem
  implicit val timeout = Timeout(5 seconds)

  val httpListener = akkaSystem.actorOf(HttpListener.props)
  IO(Http) ? Http.Bind(httpListener, interface = "localhost", port = 8080)

  sys.addShutdownHook {
    println("Signal !")
  }
}
