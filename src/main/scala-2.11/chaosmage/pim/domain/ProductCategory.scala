package chaosmage.pim.domain

case class ProductCategory(sku: Sku, category: Category)
