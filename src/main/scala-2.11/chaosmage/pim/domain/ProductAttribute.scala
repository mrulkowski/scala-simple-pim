package chaosmage.pim.domain

import java.util.Locale

case class ProductAttribute(owner: Sku, locale: Locale, code: AttributeCode, value: AttributeValue)
