package chaosmage.pim.domain

import java.util.Date

/**
  * Marker trait describing classes as a valid attribute value representations
  */
trait AttributeValue

case class BooleanAttributeValue(value: Boolean) extends AttributeValue
case class IntegerAttributeValue(value: BigInt) extends AttributeValue
case class DateAttributeValue(value: Date) extends AttributeValue
case class StringAttributeValue(value: String) extends AttributeValue
case class DecimalAttributeValue(value: BigDecimal) extends AttributeValue

