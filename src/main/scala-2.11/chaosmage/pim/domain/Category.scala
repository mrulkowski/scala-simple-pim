package chaosmage.pim.domain

case class Category(code: String, parentCode: String)
