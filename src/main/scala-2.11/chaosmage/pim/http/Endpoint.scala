package chaosmage.pim.http

import spray.routing.{Directives, Route}

trait Endpoint extends Directives {
  def route: Route
}
