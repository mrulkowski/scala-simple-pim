package chaosmage.pim.http

import akka.actor.{Actor, ActorRefFactory, Props}
import chaosmage.pim.app.components.SystemComponent
import chaosmage.pim.http.api.IndexEndpoint
import spray.routing._
import spray.util.LoggingContext

import scalaz.NonEmptyList

object HttpListener {
  def props(implicit system: SystemComponent) = Props(new HttpListener(NonEmptyList(
    new IndexEndpoint()
  )))
}

class HttpListener(endpoints: NonEmptyList[Endpoint]) extends Actor with HttpService {

  implicit def actorRefFactory: ActorRefFactory = context

  val routes = endpoints.map(_.route)
  val routing = combine(routes.head, routes.tail.toList)

  implicit val rh = RejectionHandler.Default
  implicit val rs = RoutingSettings.default(actorRefFactory)
  implicit val ex = ExceptionHandler.default
  implicit val lc = LoggingContext.fromActorContext(context)

  def receive = runRoute(routing)

  private def combine(x: Route, xs: List[Route]): Route = {
    xs match {
      case y :: ys => combine(x ~ y, ys)
      case Nil ⇒ x
    }
  }
}



