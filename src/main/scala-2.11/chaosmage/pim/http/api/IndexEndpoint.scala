package chaosmage.pim.http.api

import chaosmage.pim.http.Endpoint
import spray.routing.Route

class IndexEndpoint extends Endpoint {
  override def route: Route = pathSingleSlash {
    complete("Home page")
  }
}
