package chaosmage.csv

import org.scalatest.{Matchers, WordSpec}

class CsvParserSpec extends WordSpec with Matchers {

  "Csv parser" should {
    val reader = new CsvParser(CsvConfiguration('\'', ','))
    "return empty list for empty input" in {
      reader.parse("") shouldEqual CsvLine()
    }

    "split simple csv into list of elements" in {
      reader.parse("value1,value2") shouldEqual CsvLine("value1", "value2")
    }

    "be able to handle single column" in {
      reader.parse("v") shouldEqual CsvLine("v")
    }

    "ignore delimiter in enclosed column" in {
      reader.parse("val1,'val2,and,half'") shouldEqual CsvLine("val1", "val2,and,half")
    }

    "enclose string could be in column if it's duplicated" in {
      reader.parse("'story about ''king''','dog'") shouldEqual CsvLine("story about 'king'", "dog")
    }

    "spaces outside of columns are handled in expected manner" in {
      reader.parse(" 'value' , someOtherValue ,'test ' ") shouldEqual CsvLine("value", "someOtherValue", "test ")
    }

    "handle empty fields either enclosed or not" in {
      reader.parse(",'',") shouldEqual CsvLine("", "", "")
    }

    "handle enclosed columns with new line character" in {
      val multiLine = List("'value','Some", "Stuff', 'Finish", "not yet',lastValue", "'value','of next line'").toStream

      reader.parse(multiLine) shouldEqual List(
        CsvLine("value", "Some\nStuff", "Finish\nnot yet", "lastValue"),
        CsvLine("value", "of next line")
      ).toStream
    }

    "fail silently in case of malformed columns by new lines" in {
      reader.parse("'value','unclosed") shouldEqual CsvLine()
    }
  }
}
