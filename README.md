# README #

### Build status ###

[ ![Codeship Status for mrulkowski/scala-simple-pim](https://codeship.com/projects/7302fe90-9d30-0133-de66-62bb974e8acf/status?branch=master)](https://codeship.com/projects/127440)

### What is this repository for? ###

This repository is intended as a experiment to create reactive application that handle product marketing data in addition to data received from legacy ERP system and could it distribute to different systems (either by messages or created batch files). This is a project that I use to experiment with different solutions and to learn new technology.